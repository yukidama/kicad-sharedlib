# Shared KiCAD Library for my Projects

This reporitory contains the following global libraries shared by all my projects:

- Footprints (.pretty)
  - **AL_common**: Devices
  - **AL_conn**: Special Connectors
  - **AL_FPC**: FPC connectors and FPC gold fingers
  - **DFR (DEPRICATED)**: Schlitzblende DFR project
  - **footprint (DEPRICATED)**: Wenting's footprints.

- Symbols (.kicad_sym)
  - **AL_common**: Devices
  - **AL_conn**: Special Connectors
  - **DFR (DEPRICATED)**: Schlitzblende DFR project
  - **symbols (DEPRICATED)**: Wenting's symbols

Before using, please define the following KiCAD environment variables:

```text
AL_SHARED = *the location of this repository*
AL_SHARED_MODELS = ${AL_SHARED}/3D
```
